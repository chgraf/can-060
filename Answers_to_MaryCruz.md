CAN-060 Version 2.2 from 29th October 2018 - Comments by Mary-Cruz

Thank you for the careful reading and the comments.

All minor corrections are implemented as suggested. Find some answers for the
more involved comments in the following:

<span style="color:blue"> Any words about which type of particles and energy
ranges are going to be used, either in section 2 or 3? </span>

List of particles and energies analyzed added to section 2.


<span style="color:blue">increased spacing or having in the middle some empty slots n between absorber layers?</span>

Indeed there are empty slots in the absorber (thus more material) between the big HBU modules.
I made this more clear in the text.


<span style="color:blue"> I imagine this is not done with the scintillators but using the AHCAL
but from this sentence it looks as if the scintillators were used. There is no clear difference
between trigger info and HCAL, and there it is not clear how it is possible to disentangle two
particles crossing the same time. </span>

In order to exclude double particle events the time information of the AHCAL is used,
as those events often are separated in time by several 100ns. For this it is looked
for events that have many hits in a certain time window later than the trigger signal.
This is specified now in the text.


<span style="color:blue"> How are the MIP signals established? Any reference for example?
</span>

The energy reconstruction is done with the standard procedure - a reference is added.


<span style="color:blue">is it possible to have some clarification about why this
values and what it means edge effects in this case? </span>

The TDC ramp may not be linear at the beginning and the end. Furthermore at the end
of the ramp there is an electronics problem with the validation of events leading to
a short period of no validated events (gap in TDC spectrum, see Figure 1a) and a short period of
all hits accepted (peak at the end of the TDC spectrum). Thus a cut is applied to
reject these regions of the TDC ramp. The values are selected thus with some safety margin
we stay out of these effects.

Explanations added to section 4. And the features of the TDC spectrum (gap and peak at the end)
are now explained in the caption of the figure.


<span style="color:blue">In Sec 3 is written that 2 scintillators in front of HCAL
are used in coincidence for providing a t0-channel and then the event is declared valid
if there are hits in both REMAINING t0 channels. It were 2 NOT working perhaps? </span>

The coincidence signal of the two trigger scintillators (in front of the AHCAL) is
propagated to a total of six channels on the HBUs (i.e., the scintillating tiles & SiPMs
were removed and the channel was used to read in the PMT signal of the trigger signal).
Of these six channels, only two were working.

I tried to make this more clear in the text.

<span style="color:blue"> Since the latest times come from the outer part of the
shower, how well these models describe the transversal shape of the shower? Do
you know how is the correlation of both effects (time & shape; time & number of hits)?
The hits "migrate" the times or the hits are missing?</span>

The lateral shower shapes are well described by the physics lists, with the
exception of QBBC for the late hits, which sees slightly more
of the late hits near the center than the outer part compared to data and the other
lists. This means that indeed the late hits are "extra" for QGSP_BERT(\_HP) models
and "missing" for QBBC.

As this is an interesting question I added a paragraph to section 5.3 and the
corresponding plots to appendix B.
