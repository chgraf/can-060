CAN-060 Version 1 from 4th May 2018 - Comments by Hans-Christian

Thank you for the careful reading of the first version of the note and the comments.

<span style="color:blue">
If I understand it correctly, it
is mainly for internal use in order to publicise the results within
the collaboration. The experimental setup part is not yet written (at
least in the version I have), but nevertheless, I think some extra
sentences in the existing text here and there would help the reader;
there are several things where one has to guess what is meant; e.g.:
</span>

You are correct, the purpose of the note is to publish the results within the collaboration. The note is now complete with an introduction and experimental setup. Together with a few extra sentences, I hope that this helps to make the note more clear.

Following are answers to your suggestions.

<span style="color:blue">
  l19/l20: sentence confusing as it is not clear what is
           meant by injection of a signal; also slang:
           output soldered …;
</span>

I added a sentence to be more precise

<span style="color:blue">
  l26:     I did not get where the 3920 ns come from;
</span>

I made this more explicit

<span style="color:blue">
  l63:     average hit time …; w.r.t what? Not really
           explained what hits are used and what the
           reference is …
</span>

Hit times are always with respect to the T0 reference time. I made this more clear in the definition of hit times.

<span style="color:blue">
  fig3:    Define dT
</span>

Renamed the axis label and defined the hit time relative to the T0 reference time

<span style="color:blue">
  l110:    The slight broadening: not so clear why
           this explains the shape … but maybe I just
           don’t see it. The MC distributions don’t
           show it, right?
</span>

You are right, the explicit plots on the hit distributions comparing data and MC are not shown. However, in the reanalysis this problem is fixed now and data / MC are in much better agreement due to a better simulation of the beam line / beam smearing.


<span style="color:blue">
But, in general, it is a nice note and one can follow what has been
done and what the results show; so for internal use only it is fully
sufficient. Personally, I would have put a bit more effort in describing
the experimental and calibration procedures (even if there might be
described in [2]) as the observations have in parts already been seen
by T3B. I always think, notes also should contain everything required
such that they can be read mostly without looking at references.
</span>

More information is now given in the "experimental setup" and "event and hit selection" sections.
