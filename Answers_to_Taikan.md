CAN-060 Version 1 from 4th May 2018 - Comments by Taikan


Thank you for the careful reading of the first version of the note and the comments.

Please find answers to your comments below:

<span style="color:blue">
I think the paper is generally in a good looking.
</span>

<span style="color:blue">1. At L21 you wrote there are five T0 channels. Are they from the same scintillator or you have five scintillators?
</span>

There are two trigger scintillators and the coincidence signal is soldered on six
channels. This has been corrected and better described in the experimental setup section.

<span style="color:blue">2. I think you should show the linearity of TDC spectrum of T0 channels.
The correlation between T0 and TDC of the AHCAL channels is shown in
Figure 2(a) but this does not show that the T0/TDC is linear to the real time
and not distorted identically.
</span>

There is no way I know of to show the linearity of the T0 channels with the data. As this is our
time reference for this particular test beam there is no other reference where we can check
the linearity of the T0s against. However, in later test beam campaigns were the time reference
was given by the Beam Interface Board (BIF), we observed non-linearities of the
channels in the order of what we observed before. This leads to the assumptions, that there
is no severe non-linearity in the T0-channels.

<span style="color:blue">3. In the Fig. 1(a), TDC spectrum is shown, which ranges 800-3100,
but your selection 1. says it's cut at 500-3000, which does not seem consistent.
</span>

There are two different procedures: First the T0-channels are calibrated exploiting
the TDC spectrum in Fig. 1(a). With calibrated T0-channels this leads to a reference time
for each event. Second, for all other channels a linear regression between the reference time
and the TDC values of this channel is done to calibrate it. The 500 to 3000 TDC range
refers to the second procedure illustrated by the fit in Fig 2(a).
I hope this gets more clear with the information in the additional chapters.

<span style="color:blue">4. In the Fig. 3, I think error bars of each point should be shown
since you fit it. Chi2 should not be calculated properly without error bars.
</span>

Added statistical errors.

<span style="color:blue">5. Fig. 5 should be for electrons? Fig. 6 and 5 should be flipped if you mension
Fig. 6 earlier.
</span>

Corrected the caption.
