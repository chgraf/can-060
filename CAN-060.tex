\documentclass{JINST}
%\documentclass[final,1p,times]{elsarticle}
\let\ifpdf\relax
%\usepackage{xcolor}
\usepackage{acronym}
\usepackage{afterpage}
\usepackage{amsmath}
\usepackage[toc,page]{appendix}
\usepackage{bibentry}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{cleveref}
\usepackage{flafter}
\usepackage{float}
\usepackage{graphicx}
\usepackage{lineno}
\usepackage{microtype}
\usepackage[super]{nth}
\usepackage{paralist}
\usepackage[section]{placeins}
\usepackage{siunitx}
\usepackage{subcaption}
\usepackage{textgreek}

\def\code#1{\texttt{#1}}

\preprint{CALICE Analysis Note CAN-060 \\ v.2.3 \\January 18th, 2019}

\renewcommand{\thefootnote}{\arabic{footnote}}
\renewcommand{\floatpagefraction}{0.95}

\captionsetup[subfigure]{subrefformat=simple,labelformat=simple}
    \renewcommand\thesubfigure{(\alph{subfigure})}

\title{Time Analysis of the Partially Equipped CALICE Analog Hadronic Calorimeter Technological Prototype with Tungsten Absorber}

\author{The CALICE Collaboration\thanks{Corresponding authors: Christian Graf (cgraf@mpp.mpg.de), Frank Simon (fsimon@mpp.mpg.de) } \\
  \vspace{10 mm}

  {\rm \bf \normalsize This note contains preliminary CALICE results, and is for the use of members of the CALICE Collaboration and others to whom permission has been given.}
  }

\abstract{This note presents the analysis of timing measurements obtained with a partially instrumented
technological prototype of the CALICE analog hadronic calorimeter (AHCAL) in a
tungsten absorber stack. The data was taken during a test beam campaign in August 2015 at the
SPS at CERN. A calibration procedure for time measurements is presented and the complex
time structure of hadronic showers is analyzed and compared to several Geant4 physics lists.
For pions, late energy depositions are observed as expected due to the capture of low energetic neutrons.
Physics lists using the binary cascade to model low energetic hadronic processes underestimate
these late energy depositions, while physics lists using the Bertini model or the high precision
package overestimate them by about a factor two. The late component of hadronic showers
is found to be about ten times larger than for a similar analysis with a steel absorber.}


\begin{document}

\acrodef{ASIC}{application-specific integrated circuit}
\acrodef{AHCAL}{CALICE scintillator-SiPM hadronic calorimeter}
\acrodef{EBU}{Ecal Base Unit}
\acrodef{FWHM}{full width at half maximum}
\acrodef{HBU}{Hcal Base Unit}
\acrodef{MC}{Monte Carlo}
\acrodef{MPPC}{Multi-Pixel Photon Counter}
\acrodef{RMS}{root mean squared}
\acrodef{SiPM}{Silicon Photomultiplier}
\acrodef{PMT}{Photomuliplier Tube}

\acused{ASIC}
\acused{FWHM}
\acused{MPPC}
\acused{RMS}
\acused{SiPM}
\acused{PMT}

\linenumbers

\section{Introduction}
\label{sec:Intro}
The aim of this analysis is to establish a time calibration procedure for
the CALICE AHCAL technological prototype, and to study the complex time structure
of hadronic showers with this device in a tungsten absorber. The time calibration
is similar to \cite{CAN-061}, but differs in important parts.\\
As the CALICE AHCAL physics prototype was not capable of timing measurements, the
T3B experiment \cite{Simon2013} did first time measurements parasitic to the
physics prototype with a limited number of channels of comparable granularity. For
the technological prototype, the ability of time measurements is available for the first
time in the whole calorimeter of an CALICE AHCAL prototype. The two test beam campaigns
in July and August 2015 at the SPS at CERN were the first opportunity to test these
abilities in an environment with high energetic particles. While for the July 2015 test
beam campaign a steel absorber stack was used, the August 2015 campaign, which this
analysis focusses on, used a tungsten absorber stack.


\section{Experimental Setup}
\label{sec:Setup}
The experimental setup consists of 15 active layers in a tungsten absorber stack, which has
40 equally spaced absorber layers of \SI{10}{\mm} tungsten and \SI{0.5}{\mm} steel support \cite{tungstenAbsorber, adloff2014shower}.
The first three active layers are single \acp{EBU} with scintillator strips of
\SI[product-units=single]{4.5 x 0.5 x 0.2}{\cm^3} size instrumented by
Hamamatsu \acp{MPPC}. The first \ac{EBU} layer is placed in front of the first tungsten
absorber. For the present analysis, the \ac{EBU} data is not considered. Eight layers
of single \ac{HBU} layers with \SI[product-units=single]{3 x 3 x 0.3}{\cm^3} scintillator tiles are placed behind the \acp{EBU}.
Furthermore, four big layers consisting of four \acp{HBU} each, are placed with an increased
number of empty absorber slots after the single \ac{HBU} layers. For the \acp{HBU}, depending on the layer,
scintillator tiles with and without wavelength shifting fibers, as well as different \acp{SiPM}
are used. A detailed list of the configuration of active layers can be found in \cref{sec:LayerConfiguration}.\\
Two small and one big trigger scintillators were placed in front of the AHCAL and one big
trigger scintillator was placed behind the AHCAL. Additionally, a Cherenkov detector was
present in the beam line for particle identification. The coincidence signal of the small
trigger scintillators is used as a validation signal and as a time reference. For this
purpose six of the channels in the outer part of the big \acp{HBU} are used to read
out the coincidence signal of the \acp{PMT} instrumenting the trigger scintillators, instead
of the \acp{SiPM}. These channels are called $T_0$-channels. Initially there were six
$T_0$-channels foreseen. For the test beam campaign that is discussed in this note, only
two of them were working.\\
Data of \SI{120}{\GeV} muons, \SI{20}{\GeV} electrons and \SIlist[list-units = single]{10;30;50;70;90}{\GeV} pions
are analyzed. The energy reconstruction follows the standard reconstruction procedure
and uses the calibration constants that are used in \cite{EldwanThesis}.

\section{Event and Hit Selection}
This section briefly describes the event selection for the present analysis.
For the validation of events, the $T_0$-channels representing the coincidence of two
of the trigger scintillators in front of the AHCAL are used. An amplitude cut on
the $T_0$-signals is applied in order to reduce noise in these channels. An event is
declared valid if it also has hits in both remaining $T_0$-channels that are within \SI{5}{ns}
(after calibration, see \cref{sec:TimeCali:Ref}).
Events that involve two particles from the beam are rejected by identifying hit clusters
in the time dimension based on the AHCAL time information.
On all hits, a \SI{0.5}{\text{MIP}} cut is applied. As layer 11 was found to be noisy,
it is excluded throughout the whole analysis.\\
For Muons, a track is required and a maximum of \SI{20}{\text{hits}} are allowed to
ensure non-showering particles. Electrons and hadrons can be distinguished by exploiting the different number of hits
in the showers and the position of the shower, as hadrons are expected to shower later
than electrons. For this the center of gravity in $z$-direction is used as a discriminative
variable. Additionally, a hit in the Cherenkov detector is required.

\clearpage

\section{Time Calibration}
\label{sec:TimeCali}
Time measurements with the CALICE AHCAL technological prototype are produced in the
readout ASIC\footnote{SPIROC2b \cite{di2013spiroc}} by a voltage that is ramping up
over time with a periodicity of \SI{4}{\micro\second}. Upon a hit, the current height
of the ramp is stored in one of 16 memory-cells and at the end of a read-out cycle digitized
by a 12-bit ADC to TDC-values. As the TDC may not be linear at the beginning and at the end of the ramp,
only events are selected with the reference time between \SIrange{500}{3000}{\ns}
in order to avoid these edge effects.\\
The time calibration of the detector consists of several steps.
In a first step the reference time, given by the external trigger, needs to be calibrated.
Afterwards, the hit times are converted from TDC-values to nanoseconds and further
corrections are applied. Each of these steps are described in this section in detail.

As high energetic muons are expected to give quasi-instantaneous energy depositions
in the scintillator, muons that go through the detector without the production of
any shower are selected for calibration.

\subsection{Calibration of Reference Time}
\label{sec:TimeCali:Ref}
The aforementioned $T_0$-channels are used as a time reference. The calibration of
the time reference is done similar to \cite{CAN-061} and will only
be described briefly in the following:

The pedestal and the slope of the voltage ramps can be extracted by the
TDC-spectra (see e.g., \Cref{fig:t0spectrum}). The pedestal is given as the
start of the spectrum and the slope can be calculated using the width of the spectrum
and the time the ramp needs to reach its maximum (\SI{3920}{\nano\second}, for a ramp up time of
\SI{4}{\micro\second} assuming \SI{2}{\percent} dead-time):
\begin{equation}
    \text{slope [ns / TDC]} = \frac{\SI{3920}{\ns}}{\text{Max [TDC]} - \text{Pedestal [TDC]}}.
\end{equation}
The pedestal is determined for each memory-cell of the $T_0$-channels and separately for
even and odd bunch crossing IDs. The slope should be stable across the memory-cells and
channels of one ASIC. Thus, for the slope, the average of all calculated slopes for all
memory-cells is taken as the slope value for this $T_0$-channel (again separately for
even and odd bunch crossing IDs).
The time of the $T_0$-channels can now be calculated using the slope and the pedestal of the TDC-ramp:
\begin{equation}
  t_0 [\si{\nano\second}] = \left( \text{TDC} - \text{Pedestal} \right) \cdot \text{slope}.
\end{equation}
The time reference is calculated as the mean between both $T_0$-channels:
\begin{equation}
  t_{\text{reference}} = \frac{t_{0,1} + \; t_{0,2}}{2}.
\end{equation}
\Cref{fig:t0timedistribution} shows the distribution of the differences between
both $T_0$-channels. For an event to be valid in the following, the event time reported
by the two $T_0$-channels has to be within \SI{5}{\nano\second}. Additionally, an amplitude
cut is applied on the $T_0$-channels to reduce noise.

\begin{figure}[ht]
  \center
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1\textwidth]{./plots/03-Calibration/T0_example.pdf}
    \caption{}
    \label{fig:t0spectrum}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1\textwidth]{./plots/03-Calibration/t0Time_cut.pdf}
    \caption{}
    \label{fig:t0timedistribution}
  \end{subfigure}
  \caption{(a) TDC-spectrum of a representative channel. The gap and the peak in the
  spectrum between \SIrange{3000}{3500}{\ns} are due to a malfunction of the electronics.
  At the end of the TDC cycle, the validation is not working properly and all events
  are accepted leading to the peak, with a short period of no accepted events before,
  leading to the gap.(b) Distribution of differences
  between both $T_0$-channels. The red line marks the cut on the time difference}
  \label{fig:t0calibration}
\end{figure}


\subsection{Calibration of Hit Time}
\label{sec:TimeCali:Hit}

After the $T_0$-channels are calibrated, each event is assigned a reference time in
nanoseconds. The other channels could in principle be calibrated in the same scheme
(as it is done for example in \cite{CAN-061}). However, as we have already a calibrated time reference
for each hit, the correlation
between the hit's TDC-value and the reference time can be used for each channel
(and memory-cell) (see e.g., \cref{fig:fitCalibration}).
This has the advantage that less hits are necessary for the calibration and the procedure is less susceptible
to features in the TDC-spectrum.
In order to calibrate the channels, a linear fit between the hits TDC-values and
its reference times is applied. To reach the necessary precision, this is done in four steps:
\begin{enumerate}
  \item A robust fit taking \SI{80}{\percent} of the hits into account is performed. To avoid edge
  effects, only the range from 500 to 3000 TDCs is used.
  \item Every hit that differs from the prediction of the fit by more than \SI{10}{\nano\second}
  is ignored in subsequent fitting steps.
  \item As the slope of the TDC-ramp is a feature of the \ac{ASIC} and should be
  stable among channels, the average of all such determined slopes of the first memory-cells of all
  the channels in an \ac{ASIC} is used as a fixed value for another linear fit.
  \item The $y$-interception of this second fit is used as the offset value for
  this memory-cell.
\end{enumerate}
Again, even and odd bunch crossing IDs are treated separately.
This procedure requires at least 30 hits in the corresponding memory-cell
and at most 1000 hits are considered.
The hit times are then calculated with respect to the $T_0$-reference time as:
\begin{equation}
  t [\si{\nano\second}] = \text{TDC} \cdot \text{Slope [\si{\nano\second \per \, \text{tdc}} ]} + \text{Offset} \;[\si{\nano\second}] - t_{\text{reference}}.
\end{equation}

\subsection*{Non-Linearity Correction}
\label{sec:TimeCali:NonLin}
As the TDC-ramp may not be completely linear a phenomenological non-linearity correction is applied
similar to \cite{CAN-061} using a \nth{2}-order polynomial fit. \Cref{fig:nonLinCorrection}
shows the residuals of the linear fit for a representative channel together with the
fit that is used for the non-linearity correction.

\begin{figure}[ht]
  \center
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1\textwidth]{./plots/03-Calibration/fit_calibration_example.pdf}
    \caption{}
    \label{fig:fitCalibration}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1\textwidth]{./plots/03-Calibration/non_linearity_example.pdf}
    \caption{}
    \label{fig:nonLinCorrection}
  \end{subfigure}
  \caption{(a) Linear fit between TDC-values of hits and reference time of the event
    shown for one representative channel. (b) \nth{2}-order polynomial fit to the
    residuals of the linear fit for the non-linearity correction.}
  \label{fig:hitCalibration}
\end{figure}


\subsection*{Time Walk Correction}
\label{sec:TimeCali:TW}

In order to correct for the fact that signals with a higher amplitude are expected
to cross a constant threshold earlier than hits with a lower amplitude, a time walk
correction is applied. The average hit time with respect to the hit energy is presented
in \cref{fig:twCorrection}. The fit to the data is used as a correction of the hit
time depending on the hit energy.

\begin{figure}[ht]
  \center
    \includegraphics[width = 0.55\textwidth]{./plots/03-Calibration/timeWalk_correction.pdf}
  \caption{Average hit time dependent on the hit energy. The distribution is fitted with
    an exponential function of the form: $p_0+p_1*e^{p_2*x}$.}
  \label{fig:twCorrection}
\end{figure}

\section{Results}
\label{sec:Results}
This section discusses the time distribution for muon, electron and pion data. The
muon data is used to calibrate the detector and to extract the parameters for the time smearing of
\ac{MC} hits. The calibration is cross-checked with electron data and
further corrections are applied to account for the observed occupancy dependence of
the time resolution. Furthermore, the complex time structure of hadronic showers
is discussed for pion data.

\subsection{Muons}
\label{sec:Results:Muons}
The time distribution of muon hits after calibration is shown in \cref{fig:muonTimeRes}.
The time resolution is 6.3ns (\ac{RMS}, in the interval [\SI{-50}{\nano\second}, \SI{50}{\nano\second}])
, or \SI{10.3}{\nano\second} \ac{FWHM}.

The muon hit time distribution is used as an input for the time smearing of the \ac{MC}
simulation \footnote{Geant4 10.1, Mokka v08-05}. For this purpose the hit time distribution is parametrized by a double gaussian fit:
\begin{equation}
  t_{\text{muon}} = \frac{A_1}{A_1+A_2}*\text{gaus}\left(\mu_1, \sigma_1\right) + \frac{A_2}{A_1+A_2}*\text{gaus}\left(\mu_2, \sigma_2\right)
  \label{eq:timeSmearing}
\end{equation}
with the parameters specified in \cref{tab:muonTimeParameters}.

\begin{figure}[ht]
  \center
    \includegraphics[width = 0.6\textwidth]{./plots/04-Muons/muon_TimeDistribution.pdf}
  \caption{Muon hit time distribution. Comparison of data (black) and \code{QGSP\_BERT\_HP} (green).}
  \label{fig:muonTimeRes}
\end{figure}


\begin{table}[h!]
  \centering
  \caption{Parameters used as input for the time smearing of the \ac{MC} simulation with \cref{eq:timeSmearing}}
  \label{tab:muonTimeParameters}
  \begin{tabular}{rrl}
     $A_1$      & \num{66.8e3}             & $\pm$ \num{0.3e3}  \\
     $\mu_1$    & \SI{-0.14}{\nano\second}  & $\pm$ \SI{0.01}{\nano\second}\\
     $\sigma_1$ & \SI{3.61}{\nano\second}  & $\pm$ \SI{0.02}{\nano\second}\\
     $A_2$      & \num{39.8e3}             & $\pm$ \num{0.4e3} \\
     $\mu_2$    & \SI{0.24}{\nano\second} & $\pm$ \SI{0.01}{\nano\second}\\
     $\sigma_2$ & \SI{7.39}{\nano\second}  & $\pm$ \SI{0.02}{\nano\second}\\
   \end{tabular}
\end{table}


\subsection{Electrons}
\label{sec:Results:Electrons}


Electromagnetic showers are, as well as muon hits, expected to give instantaneous
signals with respect to the achievable time resolution. Thus, they can be used to
cross-check the muon time calibration. The hit time distribution for data and \ac{MC}
for \SI{20}{\GeV} electrons is shown in \cref{fig:electronTimeRes}. It is significantly broadened
compared to the \ac{MC} prediction, which is due to a problem with the readout
\acp{ASIC} that occurs for high channel occupancies in the \ac{ASIC}. This effects
manifests itself in two ways. First, a pedestal shift in the TDC-ramp is observed which
leads to a time shift (see \cref{fig:nHitsCorrection-shift}). Second, the hit time distribution is significantly broadened
for events with a high \ac{ASIC} occupancy. The shift can be easily corrected in data using
the fit shown in \cref{fig:nHitsCorrection-shift}. The broadening of the hit time distribution cannot
be corrected but has to be included in the digitization of the \ac{MC} simulation hits. In order
to take this effect into account the double gaussian fit explained in \cref{sec:Results:Muons}
is convoluted by another gaussian distribution. The width of this additional gaussian
depending on the \ac{ASIC} occupancy is shown in \Cref{fig:nHitsCorrection-broadening}
and is used as an additional parameter for the time smearing of \ac{MC} hits.
For two layers (5 and 6) these effects are so severe that their hit time distributions
cannot be recovered at all and thus they are excluded from further analysis.
Both effects, the shift and the broadening, are consistent with the observations of the
data with the steel absorber \cite{CAN-061}.

The electron hit time distribution after applying these corrections is shown in \cref{fig:electronTimeRes}.
The time resolution for electrons is \SI{10.2}{\nano\second} RMS, or \SI{19.1}{\nano\second} FWHM.

%The reason for data and \ac{MC} not fitting perfectly is due to a difference in
%the number of hits in the first layer. Probably due to an imperfect knowledge of the
%exact setup of the material in the beam line, there are significantly more hits
%in the first layer observed in data than in \ac{MC}, which results in a higher
%occupancy and thus in a broader time distribution of the hits in this layers.
%After exclusion of the first layer,
%data and \ac{MC} are in good agreement. However, as several layers are already
%lost, the first layer will be kept in the analysis.

\begin{figure}[ht]
  %\center
  %\begin{subfigure}{0.45\textwidth}
    %\includegraphics[width = 1 \textwidth]{./plots/05-Electrons/electrons_compare_data_sim_noCorrection.pdf}
    %\caption{}
    %\label{fig:electronTimeRes_nonCorrected}
  %\end{subfigure}
  %\hfill
  %\begin{subfigure}{0.45\textwidth}
    \center
    \includegraphics[width = 0.6 \textwidth]{./plots/05-Electrons/electron_timeDistribution.pdf}
  %\end{subfigure}
  \caption{Distribution of hit times of \SI{20}{\GeV} electrons for data (black) and
    \code{QGSP\_BERT\_HP} (purple), with corrections due to high
    \ac{ASIC} occupancies.}
  \label{fig:electronTimeRes}
\end{figure}

\begin{figure}[ht]
  \center
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1 \textwidth]{./plots/05-Electrons/chipOccupancy_shift.pdf}
    \caption{}
    \label{fig:nHitsCorrection-shift}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1 \textwidth]{./plots/05-Electrons/chipOccupancy_rms.pdf}
    \caption{}
    \label{fig:nHitsCorrection-broadening}
  \end{subfigure}
  \caption{Correction to the hit time depending on the \ac{ASIC} occupancy. The
  observed shift in the hit time distribution (a) is corrected for and the
  broadening of the hit time distribution (b) is implemented in the time smearing
  of the \ac{MC} hit times. A quadratic and a square root function are added to the plots
  respecively to guide the eye.}
  \label{fig:electronCorrections}
\end{figure}

\subsection{Pions}
\label{sec:Results:Pions}

The time distribution of hadronic showers is shown in \cref{fig:pions:timeResEnergies}.
Compared to electromagnetic showers discussed in
the previous section, a late tail in the distribution is visible. The production of
slow neutrons in the hadronic part of the shower leads to two processes that produce
delayed hits. On timescales of the order of a few \si{\nano\second},
neutron-proton elastic scattering is the dominant process, while energy depositions
due to neutron capture can extend to several \si{\micro\second} after the primary
particle hits the calorimeter. Taking systematic and geometric effects into account
the results are compatible with the results presented by the T3B experiment in \cite{adloff2014time}.
The distribution is consistent over the whole energy range, meaning that the fraction
of late hits does not depend on the energy of the incoming particles, as expected.
The slight broadening of the distribution between \SI{50}{\nano\second} and \SI{120}{\nano\second}
is due to the effects described in \cref{sec:Results:Electrons}. As higher energetic particles
produce more hits, the average occupancy of the \acp{ASIC} is higher in this case
resulting in larger tails of the distribution.

A comparison of data with \ac{MC} simulations is exemplified by \SI{70}{\GeV} pions for different
physics lists in \cref{fig:pions:timeResMC}. It shows a sorting of the physics lists into two categories depending on
the model that is used for low energetic particles. Physics lists as \code{QGSP\_BERT\_HP}
that rely on the Bertini model \cite{Geant4Physics} show significantly more
hits with hit times $> \SI{100}{\nano\second}$. \code{QBBC} and \code{QGSP\_BIC} use
a binary cascade model for the propagation of low energetic particles. For them
less of these late hits are observed compared to data. \code{QGSP\_BIC\_HP} that
uses the high precision neutron model shows a similar behavior as the \code{QGSP\_BERT}-like
models.

%While the \code{QBBC} physics list remains
%essentially  unchanged in the presently used Geant4 version 10.1, substantial changes have been
%implemented in the \code{QGSP\_BERT} and high precision neutron (\code{HP}) models.
%\footnote{Comment to Editorial Board: Details about the implemented models in the different physics lists and what has
%changed exactly between Geant4 version still needs to be investigated.}

\begin{figure}[ht]
  \center
  \begin{subfigure}{0.49\textwidth}
      \includegraphics[width = 1 \textwidth]{./plots/06-Pions/pion_timeDistribution_data.pdf}
      \caption{}
      \label{fig:pions:timeResEnergies}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
      \includegraphics[width = 1 \textwidth]{./plots/06-Pions/pion_timeDistribution.pdf}
      \caption{}
      \label{fig:pions:timeResMC}
  \end{subfigure}
  \caption{(a) Distribution of hit times for \SIrange{10}{90}{\GeV} pions, \SI{120}{\GeV} muons and
  \SI{20}{\GeV} electrons. Distributions are normalized to 1.
    (b) Distribution of hit times of \SI{70}{\GeV} pions for data (black)
    compared to several Geant4 physics lists and normalized to the number of hits per \SI{10}{\ns} per event.}
  \label{fig:pions:timeRes}
\end{figure}


The distribution of the fraction of hits in an event that are later than $\SI{75}{\nano\second}$ is shown
in \cref{fig:pions:fLateHits_event}. Data is compared with three physics lists: \code{QGSP\_BERT} as
a representative using the Bertini model, \code{QGSP\_BERT\_HP} using the HP package
for low energetic neutrons and \code{QBBC} relying on the binary-cascade model.
In data the most probable value is $\SI{5}{\percent}$ hits later than \SI{75}{\nano\second}.
For \code{QBBC} the distribution is slightly shifted to lower values with significantly more events
that do not have a late hit at all, while the distribution for \code{QGSP\_BERT(\_HP)} is shifted
to higher values with the most probable value being around \SI{13}{\percent} (\SI{15}{\percent}).

\Cref{fig:pions:fLateHits_layer,,fig:pions:fLateHits_radius,,fig:pions:fLateHits_mip}
show the distribution of the average fraction of late hits over the position in $z$-direction,
the hit radius (radial distance to center of gravity of the event) and the hit energy.
It can be seen that the late hits are consistently distributed over all layers.
The \code{QBBC} physics list is missing some of the late hits especially in the last layer.
Low energy depositions have a large fraction of late hits (up to \SI{8}{\percent}
for hit energies between \SI{0.5}{\text{MIP}} and \SI{1.0}{\text{MIP}}). Large
energy depositions above \SI{5}{\text{MIP}} are again dominated by the electromagnetic part
of the shower and thus show nearly no content of late energy depositions. While the center
of the shower is dominated by the quasi-instantaneous electromagnetic shower,
the outer part of the shower consists to a large extent of late energy depositions
(up to \SI{25}{\percent} for data at hit radii close to \SI{300}{\milli\meter}).
The \code{QGSP\_BERT(\_HP)} physics lists have consistently more late hits distributed
over all hit radii and hit energies compared to data, while \code{QBBC} is missing
late hits mostly for low energy depositions ($< \SI{3}{\text{MIP}}$) at large
hit radii ($> \SI{120}{\milli\meter}$).\\
\Cref{fig:pions:Appendix:hitRadius} in \cref{sec:LateralShowerShape} shows the lateral shower shape as well as the
hit radius distributions for early and late hits. It can be seen that the instantaneous
part is well described by all physics lists.
Also the shape of the hit radius distribution for late hits agrees between data and
the \code{QGSP\_BERT(\_HP)} physics lists. The \code{QBBC} physics list however has
more late hits in the center of the shower and less in the outer part.

\begin{figure}[ht]
  \center
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1 \textwidth]{./plots/06-Pions/compare_pion_data_MC_reduced_fLateHits_event.pdf}
    \caption{}
    \label{fig:pions:fLateHits_event}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1 \textwidth]{./plots/06-Pions/compare_pion_data_MC_reduced_fLateHits_layer.pdf}
    \caption{}
    \label{fig:pions:fLateHits_layer}
  \end{subfigure}

  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1 \textwidth]{./plots/06-Pions/compare_pion_data_MC_reduced_fLateHits_radius.pdf}
    \caption{}
    \label{fig:pions:fLateHits_radius}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1 \textwidth]{./plots/06-Pions/compare_pion_data_MC_reduced_fLateHits_mip.pdf}
    \caption{}
    \label{fig:pions:fLateHits_mip}
  \end{subfigure}
  \caption{(a) Distribution of the fraction of hits in each event that are later than \SI{50}{\nano\second},
    the fraction of late hits distributed over layers (b), hit radii (c) and hit energies (d). Comparison
    of data (black) with \code{QGSP\_BERT} (light red), \code{QGSP\_BERT\_HP} (dark red)
    and \code{QBBC} physics list (green).}
  \label{fig:pions:lateHitsDistributions}
\end{figure}


\subsubsection*{Comparison to Steel Absorber}
\label{sec:Results:Pions:Steel}

As tungsten has a higher atomic number than steel, more hadronic interactions are
expected with a tungsten absorber compared to a steel absorber leading to
more produced low energetic neutrons and thus to more late hits.
The time analysis of the technological prototype with a steel absorber is described in
\cite{CAN-061}. \Cref{fig:pions:steel_tungsten} shows the time distribution of \SI{50}{\GeV}
pions with a tungsten and steel absorber. In tungsten
the late tail of the distribution is enhanced by about a factor ten.

\begin{figure}[ht]
  \center
    \includegraphics[width = 0.5\textwidth]{./plots/Steel-Tungsten-Comparison/steel-tungsten_comparison.pdf}
  \caption{Comparison of the time distribution of \SI{50}{\GeV} pions between data
   with a tungsten absorber (blue) and a steel absorber (black).}
  \label{fig:pions:steel_tungsten}
\end{figure}


\subsubsection*{Comparison to the T3B Experiment}
\label{sec:Results:Pions:T3B}
In T3B, good agreement is observed for \code{QBBC} and
\code{QGSP\_BERT\_HP} with a tungsten absorber, while a large discrepancy for late hits is seen for \code{QGSP\_BERT},
based on simulations with Geant4 9.4 \cite{simon2013time}. In the presently used Geant4 version 10.1
substantial changes have been implemented in nearly all physics lists. The neutron capture
cross-sections and the neutron final state model of \code{QBBC} have been adopted by
\code{QGSP\_BIC}, \code{QGSP\_BERT} and \code{FTFP\_BERT}. Also \code{QBBC} itself underwent
changes especially in the low and medium energy regions (< \SI{12}{\GeV}) for hadronic
interactions \cite{alberto-ribon}.\\
In addition, the T3B experiment had a special geometry. It was located at the very end
of the physics prototype probing on average a different depth of the shower, as well as
giving more weight to hits near the center of the shower, due to the strip geometry
of the experiment. As a cross-check, a toy Monte Carlo experiment was set up to
check the influence of the different Geant4 versions as well as geometric effects.
Even though it is difficult to directly compare the two experiments due to many
systematic differences, the observed results of the toy \ac{MC} are consistent
with the differences between the T3B results and the analysis presented in this note.


\section{Conclusion}
This note analyzes data of the AHCAL test beam campaign in August 2015 at the SPS
at CERN. The focus of this analysis lies on the time measurements of hadronic
showers with tungsten as the absorber material. For this a time calibration procedure
is established. For muons a time resolution of about \SI{6}{\ns} is achieved. The
muon time resolution is used as a time smearing for \ac{MC} simulation. For electrons
a worse time resolution of \SI{10}{\ns} is observed, as an electronics effect related
to the occupancy of the read-out \acp{ASIC} is worsening the resolution significantly.\\
For pions late, low energy hits are visible as it is expected due to low energetic
neutrons. In comparison with \ac{MC} simulation of several physics lists,
it turns out that models that rely on the binary cascade for hadronic processes of
low energetic particles are underestimating these late energy depositions, while
models that use the Bertini model and the high precision neutron package are overestimating
this part by about a factor two. Comparing the results of this analysis with a similar analysis
using data with a steel absorber, significantly more late energy depositions are
observed in tungsten, which is expected because of the higher atomic number of tungsten.

\clearpage
\bibliography{CAN-060}
\bibliographystyle{JHEP}

%\end{linenumbers}
\clearpage

\begin{appendices}
  \section{Layer Configuration}
    \label[appendix]{sec:LayerConfiguration}
    \begin{table}[h!]
      \caption{Physical ordering of layers in the AHCAL tungsten technological prototype for the August 2015 testbeam campaign.
      Additionally, the SiPM and tile configuration is given.}
      \label[appendix]{table:layers}
    \begin{tabular}{lllll}
     Phys. Order & Abs. Layer & Type  & SiPM & Scintillator \\
     \hline
     1 & 0 & 1 $\times$ EBU & Hamamatsu \SI{10000}{\text{px}} & strips  \\
     2 & 1 & 1 $\times$ EBU & Hamamatsu \SI{10000}{\text{px}} & strips \\
     3 & 2 & 1 $\times$ EBU & Hamamatsu \SI{1600}{\text{px}} & strips \\
     4 & 3 & 1 $\times$ HBU & Hamamatsu \SI{1600}{\text{px}} & tiles w/ surf. mount SiPMs \\
     5 & 4 & 1 $\times$ HBU & Ketek \SI{12000}{\text{px}} & tiles w/o WLS fibres \\
     6 & 5 & 1 $\times$ HBU & Ketek \SI{12000}{\text{px}} & tiles w/o WLS fibres \\
     7 & 6 & 1 $\times$ HBU & CPTA \SI{800}{\text{px}} & tiles w/ WLS fibres \\
     8 & 7 & 1 $\times$ HBU & CPTA \SI{800}{\text{px}} & tiles w/ WLS fibres \\
     9 & 8 & 1 $\times$ HBU & CPTA \SI{800}{\text{px}} & tiles w/ WLS fibres \\
     10 & 9 & 1 $\times$ HBU & CPTA \SI{800}{\text{px}} & tiles w/ WLS fibres \\
     11 & 10 & 1 $\times$ HBU & CPTA \SI{800}{\text{px}} & tiles w/ WLS fibres \\
     12 & 11 & 4 $\times$ HBU & Ketek \SI{2300}{\text{px}} & tiles w/o WLS fibres \\
     13 & 13 & 4 $\times$ HBU & Ketek \SI{2300}{\text{px}} & tiles w/o WLS fibres \\
     14 & 21 & 4 $\times$ HBU & SenSL \SI{1300}{\text{px}} & tiles w/o WLS fibres \\
     15 & 31 & 4 $\times$ HBU & SenSL \SI{1300}{\text{px}} & tiles w/o WLS fibres
    \end{tabular}
    \end{table}

  \clearpage
  \section{Additional Plots: Lateral Shower Shape}
  \label[appendix]{sec:LateralShowerShape}

\begin{figure}[ht]
  \center
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1 \textwidth]{./plots/06-Pions/compare_pion_data_MC_reduced_hitRadius.pdf}
    \caption{Hit radius distribution}
    \label[appendix]{fig:pions:hitRadius}
  \end{subfigure}
  \hfill

  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1 \textwidth]{./plots/06-Pions/compare_pion_data_MC_reduced_hitRadius_earlyHits.pdf}
    \caption{Hit radius distribution for early hits <= \SI{75}{\ns}}
    \label[appendix]{fig:pions:hitRadiusEarlyHits}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\textwidth}
    \includegraphics[width = 1 \textwidth]{./plots/06-Pions/compare_pion_data_MC_reduced_hitRadius_lateHits.pdf}
    \caption{Hit radius distribution for late hits > \SI{75}{\ns}}
    \label[appendix]{fig:pions:hitRadiusLateHits}
  \end{subfigure}
  \caption{Hit radius distribution of data compared to MC simulation using different physics lists (a). The data is
  split up in early hits <= \SI{75}{\ns} (b) and late hit > \SI{75}{\ns} (c).}
  \label[appendix]{fig:pions:Appendix:hitRadius}
\end{figure}


\end{appendices}

\end{document}
